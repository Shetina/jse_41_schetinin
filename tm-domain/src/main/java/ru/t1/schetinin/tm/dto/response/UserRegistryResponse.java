package ru.t1.schetinin.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.schetinin.tm.dto.model.UserDTO;

@Getter
@Setter
@NoArgsConstructor
public class UserRegistryResponse extends AbstractUserResponse {

    public UserRegistryResponse(@NotNull final UserDTO user) {
        super(user);
    }

}
package ru.t1.schetinin.tm.exception.user;

public final class UserIdEmptyException extends AbstractUserException {

    public UserIdEmptyException() {
        super("User id is empty...");
    }

}

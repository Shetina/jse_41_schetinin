package ru.t1.schetinin.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.schetinin.tm.dto.model.ProjectDTO;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class ProjectListResponse extends AbstractProjectResponse {

    @Nullable
    private List<ProjectDTO> projects;

    public ProjectListResponse(@NotNull final List<ProjectDTO> projects) {
        this.projects = projects;
    }

}
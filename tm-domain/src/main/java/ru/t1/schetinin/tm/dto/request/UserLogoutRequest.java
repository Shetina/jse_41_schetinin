package ru.t1.schetinin.tm.dto.request;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public class UserLogoutRequest extends AbstractUserRequest {

    public UserLogoutRequest(@Nullable final String token) {
        super(token);
    }

}

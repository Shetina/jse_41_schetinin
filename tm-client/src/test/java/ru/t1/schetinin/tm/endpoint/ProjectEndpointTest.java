package ru.t1.schetinin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.schetinin.tm.api.endpoint.IAuthEndpoint;
import ru.t1.schetinin.tm.api.endpoint.IProjectEndpoint;
import ru.t1.schetinin.tm.api.endpoint.IUserEndpoint;
import ru.t1.schetinin.tm.api.service.IPropertyService;
import ru.t1.schetinin.tm.dto.request.*;
import ru.t1.schetinin.tm.enumerated.Status;
import ru.t1.schetinin.tm.marker.SoapCategory;
import ru.t1.schetinin.tm.dto.model.ProjectDTO;
import ru.t1.schetinin.tm.service.PropertyService;

import java.util.List;

@Category(SoapCategory.class)
public class ProjectEndpointTest {

    @NotNull
    private static final IPropertyService PROPERTY_SERVICE = new PropertyService();

    @NotNull
    private static final IAuthEndpoint AUTH_ENDPOINT = IAuthEndpoint.newInstance(PROPERTY_SERVICE);

    @NotNull
    private static final IUserEndpoint USER_ENDPOINT = IUserEndpoint.newInstance(PROPERTY_SERVICE);

    @NotNull
    private static final IProjectEndpoint PROJECT_ENDPOINT = IProjectEndpoint.newInstance(PROPERTY_SERVICE);

    @Nullable
    private static String adminToken;

    @Nullable
    private static String userToken;

    @Nullable
    private String projectId1;

    private int projectIndex1;

    @Nullable
    private String projectId2;

    private int projectIndex2;

    @BeforeClass
    public static void SetUp() {
        @NotNull final UserLoginRequest loginRequest = new UserLoginRequest();
        loginRequest.setLogin("admin");
        loginRequest.setPassword("admin");
        adminToken = AUTH_ENDPOINT.login(loginRequest).getToken();
        @NotNull final UserRegistryRequest request = new UserRegistryRequest(adminToken);
        request.setLogin("userTestLogin");
        request.setPassword("userTestPassword");
        request.setEmail("testEmail");
        USER_ENDPOINT.registryUser(request);
        @NotNull final UserLoginRequest loginUserRequest = new UserLoginRequest();
        loginUserRequest.setLogin("userTestLogin");
        loginUserRequest.setPassword("userTestPassword");
        userToken = AUTH_ENDPOINT.login(loginUserRequest).getToken();
    }

    @AfterClass
    public static void tearDown() {
        @NotNull final UserRemoveRequest request = new UserRemoveRequest(adminToken);
        request.setLogin("userTestLogin");
        USER_ENDPOINT.removeUser(request);
    }

    @Before
    public void before() {
        projectId1 = createTestProject("UserProject1Name", "UserProject1Description");
        projectIndex1 = 1;
        projectId2 = createTestProject("UserProject2Name", "UserProject2Description");
        projectIndex2 = 2;
    }

    @After
    public void after() {
        @NotNull final ProjectClearRequest request = new ProjectClearRequest(userToken);
        Assert.assertNotNull(PROJECT_ENDPOINT.clearProject(request));
    }

    @NotNull
    private String createTestProject(final String name, final String description) {
        @NotNull final ProjectCreateRequest projectCreateRequest = new ProjectCreateRequest(userToken);
        projectCreateRequest.setName(name);
        projectCreateRequest.setDescription(description);
        return PROJECT_ENDPOINT.createProject(projectCreateRequest).getProject().getId();
    }

    @Nullable
    private ProjectDTO findTestProjectById(final String id) {
        @Nullable final ProjectShowByIdRequest request = new ProjectShowByIdRequest(userToken);
        request.setId(id);
        return PROJECT_ENDPOINT.showProjectById(request).getProject();
    }

    @Test
    public void changeStatusByIdProject() {
        @NotNull final Status status = Status.COMPLETED;
        @NotNull final ProjectChangeStatusByIdRequest projectCreateRequestNullToken = new ProjectChangeStatusByIdRequest(null);
        projectCreateRequestNullToken.setId(projectId1);
        projectCreateRequestNullToken.setStatus(status);
        Assert.assertThrows(Exception.class, () -> PROJECT_ENDPOINT.changeProjectStatusById(projectCreateRequestNullToken));
        @NotNull final ProjectChangeStatusByIdRequest request = new ProjectChangeStatusByIdRequest(userToken);
        request.setId(projectId1);
        request.setStatus(status);
        Assert.assertNotNull(PROJECT_ENDPOINT.changeProjectStatusById(request));
        @Nullable final ProjectDTO project = findTestProjectById(projectId1);
        Assert.assertNotNull(project);
        Assert.assertEquals(status, project.getStatus());
    }

    @Test
    public void clearProject() {
        @NotNull final ProjectClearRequest request = new ProjectClearRequest(userToken);
        PROJECT_ENDPOINT.clearProject(request);
        @NotNull final ProjectListRequest request1 = new ProjectListRequest(userToken);
        Assert.assertNull(PROJECT_ENDPOINT.listProject(request1).getProjects());
    }

    @Test
    public void createProject() {
        @NotNull final ProjectCreateRequest request = new ProjectCreateRequest(userToken);
        request.setName("UserProject3");
        request.setDescription("DescProject3");
        @Nullable ProjectDTO project = PROJECT_ENDPOINT.createProject(request).getProject();
        Assert.assertNotNull(project);
        Assert.assertEquals("UserProject3", project.getName());
        Assert.assertEquals("DescProject3", project.getDescription());
    }

    @Test
    public void completeByIdProject() {
        @NotNull final ProjectCompleteByIdRequest request = new ProjectCompleteByIdRequest(userToken);
        request.setId(projectId1);
        Assert.assertNotNull(PROJECT_ENDPOINT.completeProjectById(request));
        @Nullable ProjectDTO project = findTestProjectById(projectId1);
        Assert.assertNotNull(project);
        Assert.assertEquals(Status.COMPLETED, project.getStatus());
    }

    @Test
    public void listProject() {
        @NotNull final ProjectListRequest request = new ProjectListRequest(userToken);
        @Nullable final List<ProjectDTO> projects = PROJECT_ENDPOINT.listProject(request).getProjects();
        Assert.assertNotNull(projects);
        Assert.assertEquals(2, projects.size());
        for (ProjectDTO project : projects) {
            Assert.assertNotNull(findTestProjectById(project.getId()));
        }
    }

    @Test
    public void showProjectById() {
        @NotNull final ProjectShowByIdRequest request = new ProjectShowByIdRequest(userToken);
        request.setId(projectId1);
        Assert.assertNotNull(PROJECT_ENDPOINT.showProjectById(request));
    }

    @Test
    public void showProjectByIndex() {
        @NotNull final ProjectShowByIndexRequest request = new ProjectShowByIndexRequest(userToken);
        request.setIndex(projectIndex1);
        Assert.assertNotNull(PROJECT_ENDPOINT.showProjectByIndex(request));
    }

    @Test
    public void removeProjectById() {
        @NotNull final ProjectRemoveByIdRequest request = new ProjectRemoveByIdRequest(userToken);
        request.setId(projectId1);
        PROJECT_ENDPOINT.removeProjectById(request);
        Assert.assertNull(findTestProjectById(projectId1));
    }

    @Test
    public void updateProjectById() {
        @NotNull final ProjectUpdateByIdRequest request = new ProjectUpdateByIdRequest(userToken);
        request.setId(projectId1);
        request.setName("newProject1Test");
        request.setDescription("newDescription1Test");
        PROJECT_ENDPOINT.updateProjectById(request);
        @Nullable final ProjectDTO project = findTestProjectById(projectId1);
        Assert.assertEquals("newProject1Test", project.getName());
        Assert.assertEquals("newDescription1Test", project.getDescription());
    }

}

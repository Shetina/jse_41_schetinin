package ru.t1.schetinin.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.schetinin.tm.dto.model.TaskDTO;

import java.util.List;

public interface ITaskRepository {

    @Insert("INSERT INTO task_table (id, name, created, description, user_id, status, project_id)" +
            " VALUES (#{id}, #{name}, #{created}, #{description}, #{userId}, #{status}, #{projectId})")
    void add(@NotNull TaskDTO task);

    @Insert("INSERT INTO task_table (id, name, created, description, user_id, status, project_id)" +
            " VALUES (#{task.id}, #{task.name}, #{task.created}, #{task.description}, #{userId}, #{task.status}, #{task.projectId})")
    void addWithUserId(@NotNull @Param("userId") String userId, @NotNull @Param("task") TaskDTO task);

    @Delete("DELETE FROM task_table WHERE user_id = #{userId}")
    void clear(@NotNull @Param("userId") String userId);

    @Select("SELECT * FROM task_table WHERE user_id = #{userId}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    @Nullable List<TaskDTO> findAll(@NotNull @Param("userId") String userId);

    @Select("SELECT * FROM task_table WHERE user_id = #{userId} AND project_id = #{projectId}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    @Nullable List<TaskDTO> findAllByProjectId(@NotNull @Param("userId") String userId, @NotNull @Param("projectId") String projectId);

    @Select("SELECT * FROM task_table WHERE user_id = #{userId} ORDER BY name")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    @Nullable List<TaskDTO> findAllOrderByName(@NotNull @Param("userId") String userId);

    @Select("SELECT * FROM task_table WHERE user_id = #{userId} ORDER BY created")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    @Nullable List<TaskDTO> findAllOrderByCreated(@NotNull @Param("userId") String userId);

    @Select("SELECT * FROM task_table WHERE user_id = #{userId} ORDER BY status")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    @Nullable List<TaskDTO> findAllOrderByStatus(@NotNull @Param("userId") String userId);

    @Select("SELECT * FROM task_table WHERE id = #{id} AND user_id = #{userId} LIMIT 1")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    @Nullable TaskDTO findOneById(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @Select("SELECT * FROM task_table WHERE user_id = #{userId} LIMIT 1 OFFSET #{index}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    @Nullable TaskDTO findOneByIndex(@NotNull @Param("userId") String userId, @NotNull @Param("index") Integer index);

    @Select("SELECT COUNT(*) FROM task_table WHERE user_id = #{userId}")
    int getSize(@NotNull @Param("userId") String userId);

    @Delete("DELETE FROM task_table WHERE user_id = #{userId} AND id = #{id}")
    void remove(@NotNull TaskDTO task);

    @Update("UPDATE task_table SET name = #{name}, created = #{created}, description = #{description}, " +
            "user_id = #{userId}, status = #{status}, project_id = #{projectId} WHERE id = #{id}")
    void update(@NotNull TaskDTO task);

}
package ru.t1.schetinin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.schetinin.tm.enumerated.Sort;
import ru.t1.schetinin.tm.enumerated.Status;
import ru.t1.schetinin.tm.dto.model.TaskDTO;

import java.util.Comparator;
import java.util.List;

public interface ITaskService {

    @NotNull
    TaskDTO add(@NotNull TaskDTO model) throws Exception;

    @NotNull
    TaskDTO add(@Nullable String userId, @NotNull TaskDTO model) throws Exception;

    void changeTaskStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status) throws Exception;

    void changeTaskStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status) throws Exception;

    void clear(@Nullable String userId) throws Exception;

    @NotNull
    TaskDTO create(@Nullable String userId, @Nullable String name) throws Exception;

    @NotNull
    TaskDTO create(@Nullable String userId, @Nullable String name, @Nullable String description) throws Exception;

    boolean existsById(@Nullable String userId, @Nullable String id) throws Exception;

    int getSize(@Nullable String userId) throws Exception;

    @Nullable
    List<TaskDTO> findAll(@Nullable String userId) throws Exception;

    @Nullable
    List<TaskDTO> findAll(@Nullable String userId, @Nullable Comparator comparator) throws Exception;

    @Nullable
    List<TaskDTO> findAll(@Nullable String userId, @Nullable Sort sort) throws Exception;

    @Nullable
    List<TaskDTO> findAllByProjectId(@Nullable String userId, @Nullable String projectId) throws Exception;

    @Nullable
    TaskDTO findOneById(@Nullable String userId, @Nullable String id) throws Exception;

    @Nullable
    TaskDTO findOneByIndex(@Nullable String userId, @Nullable Integer index) throws Exception;

    void remove(@Nullable String userId, @Nullable TaskDTO model) throws Exception;

    void removeById(@Nullable String userId, @Nullable String id) throws Exception;

    void removeByIndex(@Nullable String userId, @Nullable Integer index) throws Exception;

    void updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description) throws Exception;

    void updateByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name, @Nullable String description) throws Exception;

    void updateProjectIdById(@Nullable final String userId, @Nullable final String id, @Nullable final String projectId) throws Exception;

}
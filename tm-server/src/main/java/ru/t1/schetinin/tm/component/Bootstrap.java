package ru.t1.schetinin.tm.component;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.schetinin.tm.api.endpoint.*;
import ru.t1.schetinin.tm.api.service.*;
import ru.t1.schetinin.tm.endpoint.*;
import ru.t1.schetinin.tm.enumerated.Role;
import ru.t1.schetinin.tm.enumerated.Status;
import ru.t1.schetinin.tm.dto.model.ProjectDTO;
import ru.t1.schetinin.tm.service.*;
import ru.t1.schetinin.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

@NoArgsConstructor
public final class Bootstrap implements IServiceLocator {

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(connectionService);

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(connectionService);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectService, taskService);

    @Getter
    @NotNull
    private final IUserService userService = new UserService(propertyService, connectionService, projectService, taskService);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService(propertyService);

    @NotNull
    @Getter
    private final ISessionService sessionService = new SessionService(connectionService);

    @Getter
    @NotNull
    private final IAuthEndpoint authEndpoint = new AuthEndpoint(this);

    @Getter
    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(propertyService, userService, sessionService);

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    {
        registry(systemEndpoint);
        registry(projectEndpoint);
        registry(taskEndpoint);
        registry(userEndpoint);
        registry(authEndpoint);
    }

    private void registry(@NotNull final Object endpoint) {
        @NotNull final String host = getPropertyService().getServerHost();
        @NotNull final String port = getPropertyService().getServerPort().toString();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = "http://" + host + ":" + port + "/" + name + "?wsdl";
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }

    private void initDemoData() {
        try {
//            getSessionService().clear(getUserService().findByLogin("admin").getId());
//            getTaskService().clear(getUserService().findByLogin("admin").getId());
//            getProjectService().clear(getUserService().findByLogin("admin").getId());
//            getUserService().clear();
            getUserService().create("admin", "admin", Role.ADMIN);
            getUserService().create("test", "test", "test@test.ru");
            getProjectService().add(getUserService().findByLogin("admin").getId(), new ProjectDTO("TEST PROJECT", Status.IN_PROGRESS));
            getProjectService().add(getUserService().findByLogin("admin").getId(), new ProjectDTO("DEMO PROJECT", Status.IN_PROGRESS));
            getProjectService().add(getUserService().findByLogin("admin").getId(), new ProjectDTO("BETA PROJECT", Status.IN_PROGRESS));
            getTaskService().create(getUserService().findByLogin("admin").getId(), "MEGA TASK");
            getTaskService().create(getUserService().findByLogin("admin").getId(), "SUPER TASK");
            getTaskService().create(getUserService().findByLogin("admin").getId(), "CLEAN TASK");

        } catch (@NotNull final Exception e) {
            e.printStackTrace();
        }
    }

    public void start() {
        initPID();
        initDemoData();
        loggerService.info("*** WELCOME TO TASK MANAGER ***");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
    }

    private void prepareShutdown() {
        loggerService.info("*** TASK MANAGER IS SHOOTING DOWN ***");
    }

}